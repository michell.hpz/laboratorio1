const functions = require('./index');
const context = require('../testing/Context');


test('Http trigger', async () => {
  const request = {
    query: { name: 'Michel' }
  };

  var iteration = 1000000;
  console.time('Function #1');
  for(var i=0; i<iteration; i++){
    await functions(context, request);
  }
  console.timeEnd('Function #1');
  expect(context.res.body).toContain('M');
  expect(context.res.body).toEqual('Hello, Michel');
  //expect(context.log.mock.calls.length).toBe(1);
});